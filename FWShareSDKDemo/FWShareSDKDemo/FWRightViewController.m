//
//  FWRightViewController.m
//  FWShareSDKDemo
//
//  Created by CyonLeu on 14-8-10.
//  Copyright (c) 2014年 FlyWire. All rights reserved.
//

#import "FWRightViewController.h"
#import "FWShareManager.h"
@interface FWRightViewController ()<UIAlertViewDelegate>

@end

@implementation FWRightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(oncancel:)];
    
    [self.navigationItem setLeftBarButtonItem:cancelItem];
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onChange:)];
    
    [self.navigationItem setRightBarButtonItem:done];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onShare:(id)sender
{
    [FWShareManager showShareViewFrom:self
                         shareContent:nil
                    completionHandler:^(SocialSNSType socialType, FWResponseState reponseState, FWShareData *sourceShareData, NSError *error) {
        //
    }];
}

- (IBAction)oncancel:(id)sender
{
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)onChange:(id)sender
{
    NSLog(@"onChange");
    self.view.backgroundColor = [UIColor yellowColor];
}

- (IBAction)showNext:(id)sender
{
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"rightview"];
    [self presentViewController:viewController animated:YES completion:^{
        
    }];
}

- (IBAction)showAlertView:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享" message:@"分享一段文本" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"share", nil];
    [alertView show];
}

- (IBAction)onPush:(id)sender
{
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"rightview"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [self onShare:nil];
    }
}

@end
